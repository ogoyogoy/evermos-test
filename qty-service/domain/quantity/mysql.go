package quantity

import (
	"database/sql"
	"net/http"

	"gitlab.com/evermos-test/qty-service/connection"
	"gitlab.com/evermos-test/qty-service/model"
	"gitlab.com/evermos-test/qty-service/service/logger"
)

func GetCurrentItemQtyData(query string) (model.ItemQuantity, model.Error) {

	var (
		error = model.Error{}
		data  = model.ItemQuantity{}
	)

	err := connection.QueryRow(query).Scan(
		&data.ItemID,
		&data.StockQuantity,
		&data.ActualQuantity,
		&data.BackupQuantity,
	)

	switch err {
	case sql.ErrNoRows:
		return data, error
	case nil:
		return data, error
	default:
		logger.Error(error.Message, err)
		error.Error = err
		error.Code = http.StatusInternalServerError
		error.Message = "Failed to get item quantity data"
		return data, error
	}
}

func InsertUpdatTransaction(query string) (int, model.Error) {
	var (
		error        = model.Error{}
		lastInsertID int64
	)

	exec, err := connection.Exec(query)
	if err != nil {
		error.Code = http.StatusInternalServerError
		error.Error = err
		error.Message = "Failed to insert or update qty"
		logger.Error(error.Message, err)
	} else {
		lastInsertID, _ = exec.LastInsertId()
	}

	return int(lastInsertID), error
}
