package model

type SuccessResponse struct {
	Data       interface{} `json:"data,omitempty"`
	Pagination interface{} `json:"pagination,omitempty"`
	Code       int         `json:"code,omitempty"`
}

type ErrorResponse struct {
	Message string      `json:"message,omitempty"`
	Code    int         `json:"code,omitempty"`
	Data    interface{} `json:"data,omitempty"`
}

type Pagination struct {
	Page       int `json:"page,omitnull"`
	DataInPage int `json:"data_in_page,omitnull"`
	TotalData  int `json:"total_data,omitnull"`
	TotalPage  int `json:"total_page,omitnull"`
}
