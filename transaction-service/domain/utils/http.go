package utils

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/evermos-test/transaction-service/config"
)

// Client is an HTTP client
var Client = &http.Client{
	Timeout: time.Second * time.Duration(120),
}

// DoHTTPRequest Do sends an HTTP request and returns an HTTP response
func DoHTTPRequest(request *http.Request) (response *http.Response, err error) {

	maxHTTPRetry := config.MAX_HTTP_RETRY

	for retry := 0; retry < maxHTTPRetry; retry++ {
		response, err = Client.Do(request)
		if err != nil {
			if retry == maxHTTPRetry-1 {
				return response, fmt.Errorf("Max HTTP retry exceeded. %s", err.Error())
			}
			time.Sleep(100 * time.Millisecond)
			continue
		}
		break
	}

	return response, nil
}
