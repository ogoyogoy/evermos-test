package routes

import (
	"github.com/go-chi/chi"
	"gitlab.com/evermos-test/qty-service/controller"
)

// RegisterTransactionRoutes creates a router for the boilerplate resource
func RegisterQtyTransaction() *chi.Mux {

	routes := chi.NewRouter()
	routes.Put("/calculate", controller.CalculateCurrentQty)

	return routes

}
