package quantity

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/evermos-test/transaction-service/domain/utils"
	"gitlab.com/evermos-test/transaction-service/model"
	"gitlab.com/evermos-test/transaction-service/service/logger"
)

func CalculateQuantity(itemID int, quantityRequested int) model.Error {

	var (
		error   = model.Error{}
		fullURL = fmt.Sprintf("http://127.0.0.1:8081/v1/qty-transaction/calculate")
	)

	payloadRequest := map[string]interface{}{
		"item_id":  itemID,
		"quantity": quantityRequested,
	}

	byteBody, err := json.Marshal(payloadRequest)
	if err != nil {
		error.Code = http.StatusInternalServerError
		error.Message = "Failed to encode payload qty service"
		error.Error = err

		return error

	}

	req, err := http.NewRequest(http.MethodPut, fullURL, bytes.NewBuffer(byteBody))
	if err != nil {
		errorMessage := "Failed to create request to qty service"
		logger.Error(fmt.Sprintf("%s. %v", errorMessage, error), error.Error)
		error.Code = http.StatusInternalServerError
		error.Error = err

		return error
	}

	req.Header.Add("Content-Type", "application/json")

	resBody, err := utils.DoHTTPRequest(req)
	if err != nil {
		logger.Error(fmt.Sprintf("%v ", err), error.Error)

		error.Code = http.StatusInternalServerError
		error.Error = err

		return error
	}

	body, err := ioutil.ReadAll(resBody.Body)
	if err != nil {
		msg := "failed calculate qty service"

		logger.Error(msg, errors.New(msg))
		error.Code = http.StatusInternalServerError
		error.Message = msg
		error.Error = err
	}

	if resBody.StatusCode != http.StatusOK {
		msg := utils.AnyTypeToString(body)

		logger.Error(msg, errors.New(msg))
		error.Code = http.StatusInternalServerError
		error.Message = msg
		error.Error = errors.New(msg)

		return error
	}

	return error

}
