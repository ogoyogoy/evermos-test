package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"gitlab.com/evermos-test/qty-service/config"
	"gitlab.com/evermos-test/transaction-service/domain/utils"
)

func imitate() {

	var (
		fullURL = fmt.Sprintf("http://127.0.0.1:8080/v1/transaction/create")
	)

	for i := 0; i < 10; i++ {

		log.SetFlags(log.LstdFlags | log.Lmicroseconds)
		log.Println("Exec ", i)

		payloadRequest := map[string]interface{}{
			"item_id":  1,
			"user_id":  i,
			"quantity": 1,
		}

		byteBody, err := json.Marshal(payloadRequest)
		if err != nil {
			fmt.Println("1")

			fmt.Println(err)
			panic(1)
		}

		req, err := http.NewRequest(http.MethodPost, fullURL, bytes.NewBuffer(byteBody))
		if err != nil {
			fmt.Println("2")

			fmt.Println(err)
			panic(1)
		}

		req.Header.Add("Content-Type", "application/json")

		resBody, err := DoHTTPRequest(req)
		if err != nil {
			fmt.Println("3")

			fmt.Println(err)
			panic(1)
		}

		body, err := ioutil.ReadAll(resBody.Body)
		if err != nil {

			fmt.Println("4")

		}

		fmt.Println("response Code ", resBody.StatusCode)
		if resBody.StatusCode != http.StatusOK {
			fmt.Println("5")

			fmt.Println(err)
			fmt.Println("error message  ", utils.AnyTypeToString(body))
		}

	}
}

func main() {

	go imitate()

	go func(msg string) {
		fmt.Println(msg)
	}("going")

	time.Sleep(time.Second)
	fmt.Println("done")
}

func DoHTTPRequest(request *http.Request) (response *http.Response, err error) {

	maxHTTPRetry := config.MAX_HTTP_RETRY

	for retry := 0; retry < maxHTTPRetry; retry++ {
		response, err = Client.Do(request)
		if err != nil {
			if retry == maxHTTPRetry-1 {
				return response, fmt.Errorf("Max HTTP retry exceeded. %s", err.Error())
			}
			time.Sleep(100 * time.Millisecond)
			continue
		}
		break
	}

	return response, nil
}

// Client is an HTTP client
var Client = &http.Client{
	Timeout: time.Second * time.Duration(120),
}

// AnyTypeToString convert any type value to string
func AnyTypeToString(value interface{}) (valueString string) {
	switch valueType := value.(type) {
	case []byte:
		valueString = string(valueType)
	default:
		valueByte, _ := json.Marshal(value)
		valueString = string(valueByte)
	}
	return valueString
}
