package quantity

import (
	"encoding/json"
	"errors"
	"net/http"

	"gitlab.com/evermos-test/qty-service/domain/utils"
	"gitlab.com/evermos-test/qty-service/model"
)

func ValidateRequestTransaction(req *http.Request) (model.TransactionRequest, model.Error) {

	var (
		validated = model.TransactionRequest{}
		error     = model.Error{}
	)

	decoder := json.NewDecoder(req.Body)
	if err := decoder.Decode(&validated); err != nil {
		error.Error = err
		error.Message = utils.ParseErrorJSONDecoder(err).Error()
		return validated, error

	}

	if validated.Quantity < 1 {
		msg := "Qty must be more than 1"
		error.Error = errors.New(msg)
		error.Message = msg
		error.Code = http.StatusBadRequest
	}

	return validated, error

}

func ValidateActualQty(actualQty int, qtyRequested int) model.Error {

	var (
		error = model.Error{}
	)
	if actualQty < qtyRequested {
		msg := "Oops, item already empty"
		error.Code = http.StatusBadRequest
		error.Message = msg
		error.Error = errors.New(msg)
	}

	return error

}

func DeductQty(itemID int, requestedQty, actualQty int) model.Error {

	var (
		error = model.Error{}
	)

	query := DeductQtyQuery(itemID, requestedQty, actualQty)

	_, error = InsertUpdatTransaction(query)

	return error
}
