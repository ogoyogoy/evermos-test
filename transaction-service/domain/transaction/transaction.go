package transaction

import (
	"gitlab.com/evermos-test/transaction-service/model"
)

func InsertTransactionData(payload model.TransactionRequest) model.Error {

	query := InsertTransactionDataQuery(payload)

	_, error := InsertUpdatTransaction(query)

	return error

}

func UpdateToPaidTransaction(transactionID int) model.Error {

	query := UpdateToPaidTransactionQuery(transactionID)

	_, error := InsertUpdatTransaction(query)

	return error
}
