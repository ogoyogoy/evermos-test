package quantity

import (
	"gitlab.com/evermos-test/qty-service/model"
)

func GetCurrentItemQty(itemID int) (model.ItemQuantity, model.Error) {

	query := GetCurrentItemQtyQuery(itemID)

	itemQuantityData, error := GetCurrentItemQtyData(query)

	return itemQuantityData, error

}
