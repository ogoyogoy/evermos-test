package transaction

import (
	"net/http"

	"gitlab.com/evermos-test/transaction-service/connection"
	"gitlab.com/evermos-test/transaction-service/model"
	"gitlab.com/evermos-test/transaction-service/service/logger"
)

func InsertUpdatTransaction(query string) (int, model.Error) {
	var (
		error        = model.Error{}
		lastInsertID int64
	)

	exec, err := connection.Exec(query)
	if err != nil {
		error.Code = http.StatusInternalServerError
		error.Error = err
		error.Message = "Failed to insert or update transaction"
		logger.Error(error.Message, err)
	} else {
		lastInsertID, _ = exec.LastInsertId()
	}

	return int(lastInsertID), error
}
