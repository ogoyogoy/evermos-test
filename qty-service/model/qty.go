package model

type TransactionRequest struct {
	ItemID   int `json:"item_id"`
	Quantity int `json:"quantity"`
}

type ItemQuantity struct {
	ItemID         int `json:"item_id"`
	StockQuantity  int `json:"stock_quantity"`
	ActualQuantity int `json:"actual_quantity"`
	BackupQuantity int `json:"backup_quantity"`
}
