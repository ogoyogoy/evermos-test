package controller

import (
	"net/http"

	"gitlab.com/evermos-test/qty-service/domain/quantity"
	"gitlab.com/evermos-test/qty-service/service/response"
)

func CalculateCurrentQty(res http.ResponseWriter, req *http.Request) {

	// validate input
	inputValidated, error := quantity.ValidateRequestTransaction(req)
	if error.Error != nil {
		response.Error(res, error.Code, error.Message)
		return
	}

	// get current qty
	itemQtyData, error := quantity.GetCurrentItemQty(inputValidated.ItemID)

	// validate qty from db
	error = quantity.ValidateActualQty(itemQtyData.ActualQuantity, inputValidated.Quantity)
	if error.Error != nil {
		response.Error(res, error.Code, error.Message)
		return
	}

	// update data to qty db
	error = quantity.DeductQty(inputValidated.ItemID, inputValidated.Quantity, itemQtyData.ActualQuantity)
	if error.Error != nil {
		response.Error(res, error.Code, error.Message)
		return
	}

	response.Success(res, http.StatusOK, []string{}, nil)
}
