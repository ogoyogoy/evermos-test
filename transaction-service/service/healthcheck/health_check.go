package healthcheck

import (
	"net/http"

	"gitlab.com/evermos-test/transaction-service/connection"
	"gitlab.com/evermos-test/transaction-service/service/response"
)

// IsAlive is health check handler HTTP GET - "/health".
func IsAlive(res http.ResponseWriter, req *http.Request) {

	mysqlConnection := connection.VerifyMysqlConnection()

	serviceA := false

	var result = map[string]bool{
		"service":   true,
		"database":  mysqlConnection,
		"service_a": serviceA,
	}

	response.Success(res, http.StatusOK, result, nil)

}
