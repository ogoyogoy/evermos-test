package quantity

import (
	"fmt"
)

func GetCurrentItemQtyQuery(itemID int) string {

	queryString := fmt.Sprintf(`
		SELECT 
			item_id, stock_quantity, actual_quantity, backup_quantity
		FROM
			item_quantity
		WHERE item_id = %d
	`, itemID)

	return queryString

}

func DeductQtyQuery(itemID int, requestedQty int, actualQty int) string {

	updatedQty := actualQty - requestedQty

	queryString := fmt.Sprintf(`
		UPDATE
			item_quantity
		SET 
			actual_quantity = %d
		WHERE item_id = %d
	`, updatedQty, itemID)

	return queryString

}
