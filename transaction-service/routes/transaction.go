package routes

import (
	"github.com/go-chi/chi"
	"gitlab.com/evermos-test/transaction-service/controller"
)

// RegisterTransactionRoutes creates a router for the boilerplate resource
func RegisterTransactionRoutes() *chi.Mux {

	routes := chi.NewRouter()
	routes.Post("/create", controller.CreateTransaction)
	routes.Post("/update-paid", controller.UpdateToPaidTransaction)

	return routes

}
