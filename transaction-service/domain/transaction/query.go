package transaction

import (
	"fmt"

	"gitlab.com/evermos-test/transaction-service/model"
)

func GetCurrentItemQtyQuery(itemID int) string {

	queryString := fmt.Sprintf(`
		SELECT 
			id_item, stock_qty, actual_qty
		FROM
			item_quantity
		WHERE id_item = %d
	`, itemID)

	return queryString

}

func InsertTransactionDataQuery(payload model.TransactionRequest) string {

	queryString := fmt.Sprintf(`
		INSERT INTO 
			transaction
		(item_id, quantity_ordered, user_id, payment_status)
		VALUES
		(%d, %d, %d, 'PENDING')
	`, payload.ItemID, payload.Quantity, payload.UserID)

	return queryString

}

func UpdateToPaidTransactionQuery(transactionID int) string {

	queryString := fmt.Sprintf(`
		UPDATE
			transaction
		SET 
			payment_status = 'PAID'
		WHERE
			id = %d
	`, transactionID)

	return queryString

}
