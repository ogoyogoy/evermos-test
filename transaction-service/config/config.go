package config

import (
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

var (
	err error

	NAMESPACE           string
	DOMAIN              string
	TYPE                string
	SERVICE_NAME        string
	API_PORT            string
	LOG_LEVEL           string
	STATE               string
	POD_NAME            string
	APP_SECRET_KEY      string
	MAX_HTTP_RETRY      int
	MYSQL_HOST          string
	MYSQL_PORT          int
	MYSQL_USER          string
	MYSQL_PASSWORD      string
	MYSQL_DATABASE_NAME string
	DD_AGENT_HOST       string
	DD_AGENT_PORT       string
	DD_VERSION          string
	ENABLE_DD_TRACE     bool
	ENABLE_DD_PROFILER  bool

	DateTimeFormat     string = "2006-01-02 15:04:05"
	FullDateTimeFormat string = "2006-01-02T15:04:05Z"
)

func init() {

	godotenv.Load()

	NAMESPACE = getEnv("NAMESPACE", "staging")
	DOMAIN = getEnv("DOMAIN", "sas")
	SERVICE_NAME = getEnv("SERVICE_NAME", "evermos-poc")
	TYPE = getEnv("TYPE", "api")
	POD_NAME = getEnv("POD_NAME", "evermos-poc")
	API_PORT = getEnv("API_PORT", "3000")
	LOG_LEVEL = getEnv("LOG_LEVEL", "DEBUG")
	STATE = getEnv("STATE", "DEVELOPMENT")
	APP_SECRET_KEY = getEnv("APP_SECRET_KEY", "3128e6f5-a00c-bc4a-67cc-2ac8be9aa453")
	MAX_HTTP_RETRY, _ = strconv.Atoi(getEnv("MAX_HTTP_RETRY", "2"))
	// MYSQL database
	MYSQL_HOST = getEnv("MYSQL_HOST", "localhost")
	MYSQL_PORT, _ = strconv.Atoi(getEnv("MYSQL_PORT", "3306"))
	MYSQL_USER = getEnv("MYSQL_USER", "root")
	MYSQL_PASSWORD = getEnv("MYSQL_PASSWORD", "root")
	MYSQL_DATABASE_NAME = getEnv("MYSQL_DATABASE_NAME", "evermos-poc")

}

func getEnv(envKey string, defaultValue string) string {
	if value := os.Getenv(envKey); value != "" {
		return value
	}
	return defaultValue
}
