#### Q1: Describe what you think happened that caused those bad reviews during our 12.12 event and why it happened. 


```bash
Karena request yg banyak dlm satu waktu bersamaan, tp resource yg digunakan sama:
contohnya: 
	- ada 10 request per detik
	- misal service utk handle request yg banyak dilakukan scale replikanya menjadi 4
	- karena dilakukan scale replica, pada saat query ke db utk ambil stok 
		yg tersedia pada beberapa service replica kemungkinan resourcenya sama
		 (misal replika 2 & 3 menggunakan resource db yg sama )
	- jadi ketika update ke db bisa jadi minus, 
		karena replica 2 & 3 mengurangi item yg sama.
```

### Q2:Based on your analysis, propose a solution that will prevent the incidents from occurring again.

```bash
Saya pikir utk masalah qty minus mungkin saja tetap terjadi tapi 
	bisi dimimalisir serendah mungkin kemungkinannya.
	 
	-  dari perspektif bisnis:, jika dijual 100 pcs utk 1 item, 
		stok yg avail harus ada 110. (angka 110 ini diambil dari forecast)

    -   dari perspektif BE: saya mendapatkan pendekatan 2 yg cocok:

		1. 2PC ( Two Phase Commit Protocol )
		2. Saga Pattern

	    Pendekatan yg akan saya gunakan adalah Saga Pattern. 

	    Kenapa Saga Pattern: saya akan develop PoC ini dalam microservice.

	    Kenapa microservice: utk membuat pola dalam setiap membangun service 
	    	itu bisa disederhanakan menjadi proses yg lebih spesifik, sehingga saya rasa lebih make sense utk 
	    	dilakukan skalability dan development bisa lebih optimal karena tech stacknya bisa dibuat berbeda-beda.
        
        Gimana pendekatan Saga pattern itu? 
        proses yg tidak memiliki central jadi setiap service menghasilkan 
        dan mendengarkan event serta memutuskan apakah suatu tindakan harus diambil atau tidak.

		Sedangkan Two Phase Commit Protocol lebih cocok utk arsitektur monolitik 
		karena proses monolith mengharuskan proses crud ke beberapa tabel terkait 
		dilakukan harus commit berbarengan atau rollback berbarengan. 
```

### Q3: Based on your proposed solution, build a Proof of Concept that demonstrates technically how your solution will work.

```bash

	Untuk bisa implementasi saga pattern minimal harus ada ke-4 serivce 

	1. Transaction Service menyimpan data transaksi baru dan menentukan state atau 
		status pending dan sekaligus publish event dengan nama, misalkan TRANSACTION_CREATED.
	2. Payment Service mendengarkan event TRANSACTION_CRETED, melakukan debet pada client, 
		dan mempublish event BILLED_ORDER. Pada proses ini triger bisa di datangkan juga 
		dari proses pembayaran yang di lakukan client.
	3. Quantity service mendengarkan event BILLED_ORDER, melakukan update stock, 
		melakukan persiapan untuk pengiriman 
		product yang di beli, dan mempublish event ORDER_PREPARED
	4. Delivery Service mendengarkan event ORDER_PREPARED melakukan pengambilan dan mengirimkan product.


	Tapi utk PoC disini saya hanya akan menggunakan 2 service saja:

	1. Transcation Service

	2. Quantity Service

	Dan untuk komunikasi antar service tidak menggunakan 
	 message broker, tapi masih menggunakan http.

```


### How To run
* import db_and_schema.sql ( mysql )

### How to run service: qty-service 
> `cd qty-service`
* create .env and edit accordingly
> `cp env .env`
* Install Dependency Management
> `go get -u github.com/golang/dep/cmd/dep`

* Install Depedencies
> `dep ensure`
* Running Program
> `go run main.go`

### How to run service: transaction-service 
> cd transaction-service
* create .env and edit accordingly
> `cp env .env`
* Install Dependency Management
> `go get -u github.com/golang/dep/cmd/dep`

* Install Depedencies
> `dep ensure`
* Running Program
> `go run main.go`


### How to test
* `go run test.go`


### Test Result

> Skenarionya stok yg tersedia ada 15, user request 10 secara berbarengan

```bash
❯ go run test.go
going
2021/05/30 16:49:59.378816 Exec  0
response Code  200
2021/05/30 16:49:59.443969 Exec  1
response Code  200
2021/05/30 16:49:59.450517 Exec  2
response Code  200
2021/05/30 16:49:59.459686 Exec  3
response Code  200
2021/05/30 16:49:59.482151 Exec  4
response Code  200
2021/05/30 16:49:59.484043 Exec  5
response Code  200
2021/05/30 16:49:59.487335 Exec  6
response Code  200
2021/05/30 16:49:59.491605 Exec  7
response Code  200
2021/05/30 16:49:59.495229 Exec  8
response Code  200
2021/05/30 16:49:59.508983 Exec  9
response Code  200
done
```

>stok menjadi 5, kemudian dilakukan run lagi

```bash
go run test.go        
going
2021/05/30 16:50:42.105754 Exec  0
response Code  200
2021/05/30 16:50:42.126264 Exec  1
response Code  200
2021/05/30 16:50:42.143885 Exec  2
response Code  200
2021/05/30 16:50:42.159537 Exec  3
response Code  200
2021/05/30 16:50:42.179368 Exec  4
response Code  200
2021/05/30 16:50:42.184761 Exec  5
response Code  500
5
<nil>
error message   {"message":"{\"message\":\"Oops, item already empty\",\"code\":400}\n","code":500}

2021/05/30 16:50:42.187620 Exec  6
response Code  500
5
<nil>
error message   {"message":"{\"message\":\"Oops, item already empty\",\"code\":400}\n","code":500}

2021/05/30 16:50:42.192009 Exec  7
response Code  500
5
<nil>
error message   {"message":"{\"message\":\"Oops, item already empty\",\"code\":400}\n","code":500}

2021/05/30 16:50:42.197802 Exec  8
response Code  500
5
<nil>
error message   {"message":"{\"message\":\"Oops, item already empty\",\"code\":400}\n","code":500}

2021/05/30 16:50:42.208329 Exec  9
response Code  500
5
<nil>
error message   {"message":"{\"message\":\"Oops, item already empty\",\"code\":400}\n","code":500}

done

```











