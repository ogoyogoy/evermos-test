package main

import (
	"fmt"
	"net/http"

	"gitlab.com/evermos-test/qty-service/config"
	"gitlab.com/evermos-test/qty-service/connection"
	"gitlab.com/evermos-test/qty-service/routes"
	"gitlab.com/evermos-test/qty-service/service/logger"
)

func main() {
	defer connection.CloseMysqlClientConnection()

	var (
		port   = config.API_PORT
		router = routes.GetRouter()
	)

	logger.Info(fmt.Sprintf("Service started. Listening on port: %s", port), nil)
	err := http.ListenAndServe(":"+port, router)
	if err != nil {
		msg := fmt.Sprintf("Failed running service without TLS. Listening on port:%s bind: address already in use", port)
		logger.Fatal(msg, nil)
	}
}
