package utils

import (
	"time"

	"gitlab.com/evermos-test/qty-service/config"
)

func StrDateTimeToDatetime(strDateTime string) (time.Time, error) {
	dateTime, err := time.Parse(config.DateTimeFormat, strDateTime)
	if err != nil {
		return dateTime, err
	}
	return dateTime, nil
}

func MysqlStrDateTimeToDatetime(strDateTime string) (time.Time, error) {
	dateTime, err := time.Parse(config.FullDateTimeFormat, strDateTime)
	if err != nil {
		return dateTime, err
	}
	return dateTime, nil
}

func DateTimeToStrDateTime(dateTime time.Time) string {
	return dateTime.Format(config.DateTimeFormat)
}
