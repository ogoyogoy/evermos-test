package controller

import (
	"net/http"

	"gitlab.com/evermos-test/transaction-service/domain/quantity"
	"gitlab.com/evermos-test/transaction-service/domain/transaction"
	"gitlab.com/evermos-test/transaction-service/service/response"
)

func CreateTransaction(res http.ResponseWriter, req *http.Request) {

	// validate input
	inputValidated, error := transaction.ValidateRequestTransaction(req)
	if error.Error != nil {
		response.Error(res, error.Code, error.Message)
		return
	}

	// validate to qty service
	error = quantity.CalculateQuantity(inputValidated.ItemID, inputValidated.Quantity)
	if error.Error != nil {
		response.Error(res, error.Code, error.Message)
		return
	}

	// insert data to db
	error = transaction.InsertTransactionData(inputValidated)
	if error.Error != nil {
		response.Error(res, error.Code, error.Message)
		return
	}

	response.Success(res, http.StatusOK, []string{}, nil)
}

func UpdateToPaidTransaction(res http.ResponseWriter, req *http.Request) {

	// validate input
	inputValidated, error := transaction.ValidateUpdatePaidRequest(req)
	if error.Error != nil {
		response.Error(res, error.Code, error.Message)
		return
	}

	error = transaction.UpdateToPaidTransaction(inputValidated.TransactionID)
	if error.Error != nil {
		response.Error(res, error.Code, error.Message)
		return
	}

	response.Success(res, http.StatusOK, []string{}, nil)

}
