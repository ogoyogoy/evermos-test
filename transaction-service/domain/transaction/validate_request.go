package transaction

import (
	"encoding/json"
	"errors"
	"net/http"

	"gitlab.com/evermos-test/transaction-service/domain/utils"
	"gitlab.com/evermos-test/transaction-service/model"
)

func ValidateRequestTransaction(req *http.Request) (model.TransactionRequest, model.Error) {

	var (
		validated = model.TransactionRequest{}
		error     = model.Error{}
	)

	decoder := json.NewDecoder(req.Body)
	if err := decoder.Decode(&validated); err != nil {
		error.Error = err
		error.Message = utils.ParseErrorJSONDecoder(err).Error()
		return validated, error

	}

	if validated.Quantity < 1 {
		msg := "Qty must be more than 1"
		error.Error = errors.New(msg)
		error.Message = msg
		error.Code = http.StatusBadRequest
	}

	return validated, error

}

func ValidateUpdatePaidRequest(req *http.Request) (model.UpdatePaidRequest, model.Error) {
	var (
		validated = model.UpdatePaidRequest{}
		error     = model.Error{}
	)

	decoder := json.NewDecoder(req.Body)
	if err := decoder.Decode(&validated); err != nil {
		error.Error = err
		error.Message = utils.ParseErrorJSONDecoder(err).Error()
		return validated, error

	}

	if validated.TransactionID == 0 {
		msg := "Invalid Request"
		error.Error = errors.New(msg)
		error.Message = msg
		error.Code = http.StatusBadRequest
	}

	return validated, error

}
