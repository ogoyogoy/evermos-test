package model

type TransactionRequest struct {
	ItemID   int `json:"item_id"`
	UserID   int `json:"user_id"`
	Quantity int `json:"quantity"`
}

type UpdatePaidRequest struct {
	TransactionID int `json:"transaction_id"`
}
