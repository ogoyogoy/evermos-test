package routes

import (
	"net/http"
	"time"

	"github.com/go-chi/chi"
	chiMiddleware "github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	chitrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/go-chi/chi"

	"gitlab.com/evermos-test/transaction-service/service/healthcheck"
	"gitlab.com/evermos-test/transaction-service/service/middleware"
)

// GetRouter returns a Mux object that implements the Router interface.
func GetRouter() *chi.Mux {

	router := chi.NewRouter()

	setupMiddleware(router)

	router.Get("/health", healthcheck.IsAlive)

	// API version 1
	router.Route("/v1", func(router chi.Router) {
		// register your routes here
		router.Mount("/transaction", RegisterTransactionRoutes())
	})

	return router

}

// Setup global middlewares
func setupMiddleware(router *chi.Mux) {

	// Add a own middleware.
	router.Use(
		setCorsOptions(),
		middleware.Recovery,
		middleware.TraceIDHeader,
	)

	// Add a chi middleware.
	router.Use(
		chiMiddleware.Timeout(60*time.Second),
		chiMiddleware.StripSlashes,
		chitrace.Middleware(),
	)

}

func setCorsOptions() func(next http.Handler) http.Handler {

	// Basic CORS (cross-origin-resource-sharing)
	// for detail about CORS, see: https://github.com/rs/cors
	var corsOptions = cors.Handler(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-Api-Key", "trace_id"},
		AllowCredentials: true,
		MaxAge:           300, // results of a preflight request can be cached in 5 minutes.
	})

	return corsOptions

}
