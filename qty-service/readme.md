# Evermos Qty Service for 12.12 stcok based from Our Boilerplate API V3

## Getting Started

* Create .env and edit accordingly
> `cp env .env

## How to run or Develop in local environment

### Install Dependency

* Change directory to project
> `cd evermos-qty`

* Install Dependency Management
> `go get -u github.com/golang/dep/cmd/dep`

* Install Depedencies
> `dep ensure`

### Run with live reload
* download air
> `go get -u -v github.com/cosmtrek/air`

* create empty config to make air works
> `touch .air.conf`

* run 
> `air`

### Run with native Go

* Run Service
> `go run ./main.go`

## Examples of request and response:

```bash
curl -X GET http://localhost:3000/health
```

Import the .json file (etc/postman/) to your postman