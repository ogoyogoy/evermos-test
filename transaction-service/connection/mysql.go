package connection

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/evermos-test/transaction-service/config"
	"gitlab.com/evermos-test/transaction-service/service/logger"
)

var (
	mysqlClient *sql.DB
	err         error
)

func init() {
	logger.Info("Init MySQL Database Connection.", nil)

	mysqlDNS := getMysqlDNS()

	if mysqlClient, err = sql.Open("mysql", mysqlDNS); err != nil {
		msg := fmt.Sprintf("Failed connected to the MySQL. %v", err.Error())
		logger.Fatal(msg, nil)
	}

	if !VerifyMysqlConnection() {
		msg := fmt.Sprintf("No connection to the MySQL Database. %v", err.Error())
		logger.Fatal(msg, nil)
	}
}

func getMysqlDNS() string {

	var params string = ""

	dns := fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?%s",
		config.MYSQL_USER,
		config.MYSQL_PASSWORD,
		config.MYSQL_HOST,
		config.MYSQL_PORT,
		config.MYSQL_DATABASE_NAME,
		params,
	)

	return dns
}

// VerifyMysqlConnection Verifies a connection to the database is still alive.
func VerifyMysqlConnection() bool {
	if err = mysqlClient.Ping(); err != nil {
		logger.Error("Failed verifies a connection to the database", err)
		return false
	}
	return true
}

// CloseMysqlClientConnection close mysql database connection
func CloseMysqlClientConnection() {
	mysqlClient.Close()
}

func Query(query string) (*sql.Rows, error) {
	rows, err := mysqlClient.Query(query)
	if err != nil {
		return nil, err
	}
	return rows, nil
}

func QueryRow(query string) *sql.Row {
	return mysqlClient.QueryRow(query)
}

func Exec(query string) (sql.Result, error) {
	result, err := mysqlClient.Exec(query)
	if err != nil {
		return nil, err
	}
	return result, nil
}
